'use strict';

module.exports = function(sequelize, DataTypes) {
  var Product = sequelize.define('Product', {
    
    product_name: DataTypes.STRING,
    unit_price : DataTypes.DOUBLE,
    category_id : DataTypes.INTEGER
    
    
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Product;
};