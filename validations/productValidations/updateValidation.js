const { Joi } = require('express-validation')
const updateValidation = {
  body: Joi.object({
    product_name: Joi
      .string()
      .min(2)
      .required(),
      
    category_id : Joi
      .number()
      .required(),

    unit_price : Joi.number()
    .required()
    .greater(0)
      
  })
}


module.exports = updateValidation ;