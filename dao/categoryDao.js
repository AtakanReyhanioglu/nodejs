const db = require('../models');
var Category = db.Category
var categoryDao = {
  findAll: findAll,
  create: create,
  findById: findById,
  deleteById: deleteById,
  update: update
}

function findAll() {
  return Category.findAll();
}

function findById(id) {
  return Category.findByPk(id);
}

function deleteById(id) {
  return Category.destroy({ where: { id: id } });
}

function create(category) {
  var newCategory = new Category(category);
  return newCategory.save();
}

function update(category, id) {
  var update = {
      category_name: category.category_name,     
  };
  return Category.update(update, { where: { id: id } });
}
module.exports = categoryDao;

