const db = require('../models');

var Product = db.Product
var productDao = {
  findAll: findAll,
  create: create,
  findById: findById,
  deleteById: deleteById,
  update: update,
  getByCategoryId:getByCategoryId
}
function getByCategoryId(categoryId) {
  return Product.findAll({where : {category_id : categoryId}});
}
function findAll() {
  return Product.findAll();
}

function findById(id) {
  return Product.findByPk(id);
}

function deleteById(id) {
  return Product.destroy({ where: { id: id } });
}

function create(product) {
  var newProduct = new Product(product);
  return newProduct.save();
}

function update(product, id) {
  var update = {
      product_name: product.product_name,    
      category_id : product.category_id,
      unit_price: product.unit_price 
  };
  return Product.update(update, { where: { id: id } });
}

module.exports = productDao;

