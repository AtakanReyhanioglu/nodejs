const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');

const {validate} = require('express-validation')
const addValidation = require('../validations/productValidations/addValidation')
const updateValidation = require('../validations/productValidations/updateValidation')

router.post('/add',validate(addValidation, {}, {}), productController.add);
router.get('/getall', productController.findAll);
router.get('/getByCategoryId/:categoryId', productController.getByCategoryId);
router.get('/findbyid/:id', productController.findById);
router.put('/updatebyid/:id',validate(updateValidation, {}, {}), productController.update);
router.delete('/deletebyid/:id', productController.deleteById);

module.exports = router;