const express = require('express');
const router = express.Router();
const categoryController = require('../controllers/categoryController');

const {validate} = require('express-validation')
const addValidation = require('../validations/categoryValidations/addValidation')
const updateValidation = require('../validations/categoryValidations/updateValidation')



router.post('/add',validate(addValidation, {}, {}), categoryController.add);
router.get('/getall', categoryController.findAll);
router.get('/findbyid/:id', categoryController.findById);
router.put('/updatebyid/:id',validate(updateValidation, {}, {}) ,categoryController.update);
router.delete('/deletebyid/:id', categoryController.deleteById);

module.exports = router;