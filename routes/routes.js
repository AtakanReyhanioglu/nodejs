const express = require('express');
const router = express.Router();
const categoryRoutes = require('../routes/categoryRoutes');
const productRoutes = require('../routes/productRoutes');



router.use('/api/products', productRoutes);
router.use('/api/categories', categoryRoutes);

module.exports = router;