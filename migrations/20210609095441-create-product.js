'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      product_name: {
        type: Sequelize.STRING
      },
      category_id:{
        type: Sequelize.INTEGER,

      },
      
      unit_price: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => queryInterface.addConstraint('Products',  {
      fields:['category_id'],
      type: 'FOREIGN KEY',
      name: 'FK_category_product', // useful if using queryInterface.removeConstraint
      references: {
        table: 'Categories',
        field: 'id',
      },
      onDelete: 'no action',
      onUpdate: 'no action',
    }))
    
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Products');
  }
};