
const productManager = require('../business/productManager');
var productController = {
    add: add,
    findAll: findAll,
    findById: findById,
    update: update,
    deleteById: deleteById,
    getByCategoryId:getByCategoryId
}
function getByCategoryId(req,res){
    var categoryId=req.params.categoryId
    productManager.getByCategoryId(categoryId)
  .then((data)=>{
    res.send(data)
  })
  .catch((error)=>{
    console.log(error)
  })
}
function findById(req, res) {
    var id =req.params.id
    productManager.findById(id).
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}



function add(req, res) {

    var product = req.body;
    
    productManager.add(product).
        then((data) => {
            
            res.send(data);
            
        })
        .catch((error) => {
            console.log(error);
        });
}



function deleteById(req, res) {
    var id = req.params.id
    productManager.deleteById(id).
        then((data) => {
            res.status(200).json({
                message: "Product deleted successfully",
                success: true
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function update(req, res) {
    var product = req.body
    var productId = req.params.id
    productManager.update(product, productId).
        then((data) => {
            res.status(200).json({
                message: "Product updated successfully",
                success: true
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function findAll(req, res) {
  productManager.findAll().
        then((data) => {
            res.send(data)           
            
        })
        .catch((error) => {
            console.log(error);
        });
}

module.exports = productController;