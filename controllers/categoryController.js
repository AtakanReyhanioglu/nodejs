const categoryManager = require('../business/categoryManager')
const {ValidationError} = require('express-validation')


var categoryController = {
    add: add,
    findAll: findAll,
    findById: findById,
    update: update,
    deleteById: deleteById,
}


function add(req, res) {
    let category = req.body;
    
    categoryManager.add(category).
        then((data) => {
            
            res.send(data);
            
        })
        .catch((error) => {
            console.log(error);
        });
}

function findById(req, res) {
    categoryManager.findById(req.params.id).
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

function deleteById(req, res) {
    categoryManager.deleteById(req.params.id).
        then((data) => {
            res.status(200).json({
                
                message: "Category deleted successfully",
                success: true
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function update(req, res) {
    var category = req.body
    var categoryId = req.params.id
    var category_name = req.body.category_name
    categoryManager.update(category, categoryId).
        then((data) => {
            res.status(200).json({
                data:{"category_name" : `${category_name}`},
                message: "Category updated successfully",
                success: true
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function findAll(req, res) {
    categoryManager.findAll().
        then((data) => {
            
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}




module.exports = categoryController;