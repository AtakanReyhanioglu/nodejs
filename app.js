const express = require('express');
const app = express();
const db = require('./models');
const routes = require('./routes/routes')
const bodyParser = require('body-parser')
const { validate, ValidationError, Joi } = require('express-validation')





app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use('/',routes);
app.listen(3000, function() {
  db.sequelize.sync();
});

app.use(function(err, req, res, next) {
  if (err instanceof ValidationError) {
    return res.status(err.statusCode).json(err)
  }

  return res.status(500).json(err)
})

