const productDao = require('../dao/productDao');
var productManager = {
    add: add,
    findAll: findAll,
    findById: findById,
    update: update,
    deleteById: deleteById,
    getByCategoryId:getByCategoryId
   
}
function getByCategoryId(categoryId) {
  return productDao.getByCategoryId(categoryId);
}
function findAll() {
  return productDao.findAll();
}

function findById(id) {
  return productDao.findById(id);
}

function deleteById(id) {
  return productDao.deleteById(id);
}

function add(product) {
 
  return productDao.create(product);
}

function update(product, id) {
  
  return productDao.update(product, id)
}

module.exports = productManager;
