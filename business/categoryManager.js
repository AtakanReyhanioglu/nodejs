const categoryDao = require('../dao/categoryDao');

var categoryManager = {
    add: add,
    findAll: findAll,
    findById: findById,
    update: update,
    deleteById: deleteById,
    
   
}
function findAll() {
  return categoryDao.findAll();
}

function findById(id) {
  return categoryDao.findById(id);
}

function deleteById(id) {
  return categoryDao.deleteById(id);
}

function add(category) {
 
  return categoryDao.create(category);
}


function update(category, id) {
  
  return categoryDao.update(category, id)
}
module.exports = categoryManager;
